﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Spawner : MonoBehaviour
{
	public Wave[] waves;
	public EnemyController enemy;

	public GameObject[] spawnPoints;

	Wave currentWave;

	public static int currentWaveNumber;

	int enemysRemeiningAlive;
	int enemyRemeiningToSpawn;
	float nextSpawnTime;

	

	void Start()
	{
		nextWave();
	}

	void Update()
	{
		if (enemyRemeiningToSpawn > 0 && Time.time > nextSpawnTime)
		{
			enemyRemeiningToSpawn--;
			nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

			EnemyController spawnedEnemy = Instantiate(enemy, spawnPoints[Random.Range(0,3)].transform.position, Quaternion.identity) as EnemyController;
			spawnedEnemy.OnDeath += OnEnemyDeath;
		}
	}

	void OnEnemyDeath()
	{
		enemysRemeiningAlive--;

		if (enemysRemeiningAlive == 0)
		{
			nextWave();
		}
	}

	void nextWave()
	{
		currentWaveNumber++;
		print("Wave:" + currentWaveNumber);
		if (currentWaveNumber - 1 < waves.Length)
		{
			currentWave = waves[currentWaveNumber - 1];

			enemyRemeiningToSpawn = currentWave.enemyCount;
			enemysRemeiningAlive = enemyRemeiningToSpawn;
		}
	}

	[System.Serializable]
	public class Wave
	{
		public int enemyCount;
		public float timeBetweenSpawns;
	}

}

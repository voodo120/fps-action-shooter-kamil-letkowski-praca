﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
	[SerializeField]
	public static float playerHealth = 5f;
	public bool dead = false;

	#region
	public static PlayerManager instance;

	void Awake()
	{
		instance = this;
		playerHealth = 20f;
	}
	#endregion

	public GameObject player;

	public void HurtPlayer()
	{
		playerHealth --;
		if (playerHealth <= 0 && !dead)
		{
			Die();
		}
	}

	public void Die()
	{
		dead = true;
		//Time.timeScale = 0;
		// you can also use the following if you want the game to exit
		//Application.Quit();
		//or reload current level
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
		SceneManager.LoadScene(0);
		gameObject.SetActive(false);
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{

	public Transform muzzle;
	public Projectile projectile;
	public float msBetweenShots;
	public float muzzleVelocity;
	public Transform[] ShotgunBulletPoints;

	public WeaponController weapon;

	float nextShotTime;



	public void Update()
	{
		if ( weapon.selectedWeapon == 2 && Input.GetMouseButton(0))
		{
			ShootAutomatic();
		}
		if ( weapon.selectedWeapon == 0 && Input.GetMouseButtonDown(0))
		{
			SingleShot();
		}
		if (weapon.selectedWeapon == 1 && Input.GetMouseButtonDown(0))
		{
			TripleShot();
		}
	}

	public void ShootAutomatic()
	{
		if (Time.time > nextShotTime)
		{
			nextShotTime = Time.time + msBetweenShots / 1000;
			Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
			newProjectile.SetSpeed(muzzleVelocity);
		}
	}
	public void SingleShot()
	{
		if (Time.time > nextShotTime)
		{
			nextShotTime = Time.time + msBetweenShots / 1000;
			Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
			newProjectile.SetSpeed(muzzleVelocity);
		}
	}
	public void TripleShot()
	{
		if (Time.time > nextShotTime)
		{
			nextShotTime = Time.time + msBetweenShots / 1000;
			Projectile newProjectile = Instantiate(projectile, ShotgunBulletPoints[0].position, ShotgunBulletPoints[0].rotation) as Projectile;
			Projectile newProjectile1 = Instantiate(projectile, ShotgunBulletPoints[1].position, ShotgunBulletPoints[1].rotation) as Projectile;
			Projectile newProjectile2 = Instantiate(projectile, ShotgunBulletPoints[2].position, ShotgunBulletPoints[2].rotation) as Projectile;
			newProjectile.SetSpeed(muzzleVelocity);
			newProjectile1.SetSpeed(muzzleVelocity);
			newProjectile2.SetSpeed(muzzleVelocity);
		}
	}
}
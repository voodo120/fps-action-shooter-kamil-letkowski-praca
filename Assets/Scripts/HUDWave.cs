﻿using UnityEngine;
using TMPro;

public class HUDWave : MonoBehaviour
{
	public TextMeshProUGUI text;

	// Start is called before the first frame update
	private void Awake()
	{
		text = GetComponent<TextMeshProUGUI>();

	}

	// Update is called once per frame
	void Update()
    {
		text.text = "Wave: " + Spawner.currentWaveNumber;
	}
}

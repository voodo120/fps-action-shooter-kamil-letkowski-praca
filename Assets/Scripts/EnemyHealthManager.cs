﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{
	[SerializeField]
	private float health = 1;
	protected bool dead;
	
	public ParticleSystem deathEffect;
	
	public System.Action OnDeath;
	private void Start()
	{
	}
	public void HurtEnemy(float damageToGive)
	{
		if (damageToGive >= health)
		{
			Destroy(Instantiate(deathEffect.gameObject, transform.position, Quaternion.LookRotation(Vector3.up)) as GameObject, 2); 
		}
		health = health - damageToGive;
		if (health <= 0 && !dead)
		{
			Die();
		}
	}
	public void Die()
	{
		dead = true;
		/*if (OnDeath != null)
		{
			OnDeath();
		}
		*/
		OnDeath?.Invoke();
		Destroy(gameObject);
		ScoreManager.points += 1;
	}
}

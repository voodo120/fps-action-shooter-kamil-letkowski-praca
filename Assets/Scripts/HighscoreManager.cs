﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighscoreManager : MonoBehaviour
{
	public TextMeshProUGUI highScore;

	// Update is called once per frame
	void Update()
    {
		highScore.text = "Highscore : " + PlayerPrefs.GetInt("Highscore", 0).ToString();
	}
	private void Start()
	{

	}
}

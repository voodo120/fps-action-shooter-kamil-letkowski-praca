﻿using TMPro;
using UnityEngine;


public class HUDManager : MonoBehaviour
{

	public TextMeshProUGUI text;

	private void Awake()
	{
		text = GetComponent<TextMeshProUGUI>();
		
	}

	private void Update()
	{
		text.text = "Health: " + PlayerManager.playerHealth;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : EnemyHealthManager
{
	
	public enum State { Idle, Chasing, Attack };
	State currentState;

	Material skinMaterial;

	Color orginalColor;

	public NavMeshAgent agent;
	public float moveSpeed;
	public GameObject thePlayer;
	
	float attackDistance = 0.7f;
	float timeBetweenAttacks = 1f;
	float nextAttackTime;

	float myCollisionRadius;
	float targetCollisionRadius;

	void Start()
	{
		if (FindObjectOfType<PlayerManager>())
		{
			thePlayer = GameObject.FindGameObjectWithTag("Player");
			myCollisionRadius = GetComponent<SphereCollider>().radius;
			targetCollisionRadius = thePlayer.GetComponent<SphereCollider>().radius;
		}

		skinMaterial = GetComponent<Renderer>().material;
		orginalColor = skinMaterial.color;
		currentState = State.Chasing;

	}
	void FixedUpdate()
	{
		if (currentState == State.Chasing && thePlayer.activeInHierarchy == true)
		{
			Vector3 dirToTarget = (thePlayer.transform.position - transform.position).normalized;
			Vector3 targetPosition = thePlayer.transform.position - dirToTarget * (myCollisionRadius + targetCollisionRadius + attackDistance / 3);
			agent.SetDestination(targetPosition);
		}
		
	}

	// Update is called once per frame
	void Update()
	{
		if (Time.time > nextAttackTime && thePlayer.activeInHierarchy == true)
		{
			float sqrDstToTarget = (thePlayer.transform.position - transform.position).sqrMagnitude;
			if (sqrDstToTarget < Mathf.Pow(attackDistance + myCollisionRadius + targetCollisionRadius, 2))
			{
				nextAttackTime = Time.time + timeBetweenAttacks;
				StartCoroutine(Attack());
			}
		}

	}
	
	IEnumerator Attack()
	{
		currentState = State.Attack;
		agent.enabled = false;

		Vector3 orginalPosition = transform.position;
		Vector3 dirToTarget = (thePlayer.transform.position - transform.position).normalized;
		Vector3 targetPosition = thePlayer.transform.position - dirToTarget * (myCollisionRadius + targetCollisionRadius + attackDistance / 2);
		Vector3 AttackPosition = thePlayer.transform.position;

		float attackSpeed = 3f;
		float percent = 0;

		skinMaterial.color = Color.red;
		thePlayer.GetComponent<PlayerManager>().HurtPlayer();
		while (percent <= 1)
		{
			percent += Time.deltaTime * attackSpeed;
			//Interpolacja – metoda numeryczna polegająca na wyznaczaniu w danym przedziale tzw. funkcji interpolacyjnej, która przyjmuje w nim z góry zadane wartości, w ustalonych punktach nazywanych węzłami.
			float interpolation = (Mathf.Pow(percent,2) + percent) * 4;
			//if interpolation is 0 transform will be at orginalPosition, if 1 will be at attackposition and back to 0
			transform.position = Vector3.Lerp(orginalPosition, AttackPosition, interpolation);
			

			yield return null;
		}
		skinMaterial.color = orginalColor;
		currentState = State.Chasing;
		//if finish attacking start agent pathfinding
		agent.enabled = true;
	}
	
}

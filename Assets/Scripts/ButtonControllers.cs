﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControllers : MonoBehaviour
{
	public static bool gamePaused = false;

	public GameObject resumeButton;
	public GameObject mainMenu;

	public void PlayGame()
	{
		SceneManager.LoadScene(1);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		gamePaused = false;
		Time.timeScale = 1;
	}


	void GamePaused()
	{
		if (gamePaused)
			gamePaused = false;
		else
			gamePaused = true;
	}

	public void ExitGame()
	{
		Debug.Log("Application has quit");
		Application.Quit();
	}

	public void HighscoreScene()
	{
		SceneManager.LoadScene("Highscore");
	}

	public void MainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}



	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (gamePaused)
			{
				Time.timeScale = 1;
				gamePaused = false;
				resumeButton.SetActive(false);
				mainMenu.SetActive(false);
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
			else
			{
				Time.timeScale = 0;
				gamePaused = true;
				resumeButton.SetActive(true);
				mainMenu.SetActive(true);
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.Confined;
			}
		}
	}

	public void ResumeGame()
	{
		Time.timeScale = 1;
		resumeButton.SetActive(false);
		mainMenu.SetActive(false);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		gamePaused = SetGamePaused(gamePaused);
	}

	public bool SetGamePaused(bool gameState)
	{
		if (gameState == true)
		{
			gamePaused = false;
		}
		else
		{
			return gamePaused;
		}
		return gamePaused;
	}
}

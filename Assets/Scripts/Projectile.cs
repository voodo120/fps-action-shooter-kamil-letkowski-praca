﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	[SerializeField]
	private float speed = 10.0f;

	[SerializeField]
	private float damageToGive = 1.0f;

	[SerializeField]
	public ParticleSystem hitpointEffect;

	public void SetSpeed(float newSpeed)
	{
		speed = newSpeed;
	}

	void Update()
	{
		transform.Translate(Vector3.forward * Time.deltaTime * speed);
		Destroy(gameObject, 3.0f);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			other.gameObject.GetComponent<EnemyHealthManager>().HurtEnemy(damageToGive);
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		Destroy(Instantiate(hitpointEffect.gameObject, transform.position, Quaternion.identity) as GameObject, 1);
	}
}
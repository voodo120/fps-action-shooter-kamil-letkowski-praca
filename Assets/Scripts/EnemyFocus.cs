﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyFocus : MonoBehaviour
{

	public float lookRadius = 10f;

	public Transform target;
	NavMeshAgent agent;
	[SerializeField]
	private float nextShotTime;
	public Projectile projectile;
	public Transform muzzle;
	public float muzzleVelocity;
	[SerializeField]
	private float msBetweenShots;
	

	// Use this for initialization
	void Start()
	{
		target = PlayerManager.instance.player.transform;
		agent = GetComponent<NavMeshAgent>();
	}

	// Update is called once per frame
	void Update()
	{
		float distance = Vector3.Distance(target.position, transform.position);

		if (distance <= lookRadius)
		{
			transform.LookAt(target.position);
			SingleShot();
		}
		else if (distance >= 20)
		{
			agent.SetDestination(target.position);
		}
	}
	public void SingleShot()
	{
		if (Time.time > nextShotTime)
		{
			nextShotTime = Time.time + msBetweenShots / 1000;
			Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
			newProjectile.SetSpeed(muzzleVelocity);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, lookRadius);
	}
}

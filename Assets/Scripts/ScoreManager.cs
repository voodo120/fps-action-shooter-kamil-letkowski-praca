﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.TextCore;

public class ScoreManager : MonoBehaviour
{
	
	//public TextMeshProUGUI text;
	public static int points = 0;

	//public TextMeshProUGUI highScore;
	public TextMeshProUGUI score;


	private void Start()
	{
		//highScore.text = PlayerPrefs.GetInt("Highscore", 0).ToString();
	}

	private void Awake()
	{
		//text = GetComponent<TextMeshProUGUI>();
		points = 0;
	}

	private void Update()
	{
		//text.text = "Score: " + points;

		score.text = "Score: " + points.ToString();
		if (points > PlayerPrefs.GetInt("Highscore", 0))
		{
			PlayerPrefs.SetInt("Highscore", points);
			//highScore.text = "Highscore : " + points.ToString();
		}

	}

	private void ResetScore()
	{
		PlayerPrefs.DeleteKey("Highscore");
	}

}
